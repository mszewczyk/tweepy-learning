# README #


### What is this repository for? ###

Using this as a repository for all my learning on Tweepy library for Python. 
It may contain something useful later, but probably will be nothing more than bucket of disjointed scripts documenting my journey with this library.
Tweets made by those scripts should be visible on this account: https://twitter.com/comp_face (@comp_face)

### How do I get set up? ###

Create dontupload.py file in your project that will contain 
`
CONSUMER_KEY
CONSUMER_SECRET
ACCESS_TOKEN
ACCESS_TOKEN_SECRET
`
variables with values provided from your twitter app auth settings.

