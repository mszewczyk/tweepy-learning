import tweepy
#script that contains variables with keys and tokens for tweepy
import dontupload



# Authenticate to Twitter
#auth = tweepy.OAuthHandler("CONSUMER_KEY", "CONSUMER_SECRET")
#auth.set_access_token("ACCESS_TOKEN", "ACCESS_TOKEN_SECRET")
# regenerate all of them after changing access modifiers in twitter app settings!
auth = tweepy.OAuthHandler(dontupload.CONSUMER_KEY, dontupload.CONSUMER_SECRET)
auth.set_access_token(dontupload.ACCESS_TOKEN, dontupload.ACCESS_TOKEN_SECRET)

# Create API object
api = tweepy.API(auth)

# Create a tweet
api.update_status("Hello Tweepy")

print("Done!")